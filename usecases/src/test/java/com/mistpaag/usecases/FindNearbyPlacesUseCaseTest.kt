package com.mistpaag.usecases

import com.mistpaag.data.*
import com.mistpaag.sharedtesting.CoroutineTestRule
import com.mistpaag.sharedtesting.FakePlaceValues
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class FindNearbyPlacesUseCaseTest{
    @get:Rule
    var coroutineTestRule = CoroutineTestRule()

    lateinit var findNearbyPlacesUseCase: FindNearbyPlacesUseCase

    @RelaxedMockK
    lateinit var repository: PlaceRepository

    @Before
    fun setup(){
        MockKAnnotations.init(this, relaxUnitFun = true, relaxed = true)
        findNearbyPlacesUseCase = FindNearbyPlacesUseCase(repository)
    }

    @Test
    fun `GIVEN lat,long WHEN invokeUseCase THEN returnSuccessList`() = runBlockingTest{
        val fakeLatitude = FakePlaceValues.getLatitude()
        val fakeLongitude = FakePlaceValues.getLongitude()
        val response = FakePlaceValues.getPlaces()
        val flowResponse = flowOf(ResultData.Success(response))
        coEvery { repository.findNearbyPlaces(fakeLatitude, fakeLongitude) } returns flowResponse

        val result = findNearbyPlacesUseCase.invoke(fakeLatitude, fakeLongitude)


        result.collect { placesData->
            coVerify(exactly = 1) { repository.findNearbyPlaces(any(),any()) }
            assert(placesData.isSuccess())
            assertEquals(placesData.getSuccess()?.first(), response.first())
        }
    }

    @Test
    fun `GIVEN lat,long WHEN invokeUseCase THEN returnMessageError`() = runBlockingTest{
        val fakeLatitude = FakePlaceValues.getLatitude()
        val fakeLongitude = FakePlaceValues.getLongitude()
        val error = FakePlaceValues.getError()
        val flowError = flowOf(ResultData.Error(error))

        coEvery { repository.findNearbyPlaces(fakeLatitude, fakeLongitude) } returns flowError

        val result = findNearbyPlacesUseCase.invoke(fakeLatitude, fakeLongitude)

        result.collect { popularMoviesData->
            coVerify(exactly = 1) { repository.findNearbyPlaces(any(), any()) }
            assert(popularMoviesData.isError())
            assertEquals(popularMoviesData.getError(), error)
        }
    }
}