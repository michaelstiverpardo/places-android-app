package com.mistpaag.usecases

import com.mistpaag.data.PlaceRepository
import com.mistpaag.data.ResultData
import com.mistpaag.domain.Place
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class FindNearbyPlacesUseCase @Inject constructor(
    private val repository: PlaceRepository
) {
    fun invoke(latitude:Double, longitude: Double): Flow<ResultData<List<Place>>> {
        return repository.findNearbyPlaces(latitude, longitude)
    }
}