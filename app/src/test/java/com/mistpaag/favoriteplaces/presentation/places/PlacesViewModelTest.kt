package com.mistpaag.favoriteplaces.presentation.places

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.mistpaag.data.ResultData
import com.mistpaag.sharedtesting.CoroutineTestRule
import com.mistpaag.sharedtesting.FakePlaceValues
import com.mistpaag.usecases.FindNearbyPlacesUseCase
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class PlacesViewModelTest{
    @get:Rule
    var coroutineTestRule = CoroutineTestRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    lateinit var viewModel: PlacesViewModel

    @RelaxedMockK
    lateinit var findNearbyPlacesUseCase: FindNearbyPlacesUseCase

    @RelaxedMockK
    lateinit var observable: Observer<PlacesState>

    var slot = slot<PlacesState>()
    var mutableListStates = mutableListOf<PlacesState>()


    @Before
    fun setup(){
        MockKAnnotations.init(this, relaxUnitFun = true, relaxed = true)
        viewModel = PlacesViewModel(
            findNearbyPlacesUseCase
        ).apply {
            state.observeForever(observable)
        }
        every { observable.onChanged(capture(slot)) } answers { mutableListStates.add(slot.captured) }
    }

    @Test
    fun `WHEN invoke FindNearbyPlacesIntent THEN receivedPlacesListState`() = runBlockingTest {
        val fakeLatitude = FakePlaceValues.getLatitude()
        val fakeLongitude = FakePlaceValues.getLongitude()
        val response = FakePlaceValues.getPlaces()
        val flowResponse = flowOf(ResultData.Success(response))
        val event = PlacesIntent.FindNearbyPlaces(fakeLatitude,fakeLongitude)

        coEvery { findNearbyPlacesUseCase.invoke(fakeLatitude,fakeLongitude) } returns flowResponse

        viewModel.execute(event)

        Assert.assertEquals(2, mutableListStates.size)
        Assert.assertEquals((slot.captured as PlacesState.PlacesList).list.first(), response.first())
    }

    @Test
    fun `WHEN invoke FindNearbyPlacesIntent THEN receivedErrorState`() = runBlockingTest {
        val fakeLatitude = FakePlaceValues.getLatitude()
        val fakeLongitude = FakePlaceValues.getLongitude()
        val error = FakePlaceValues.getError()
        val flowError = flowOf(ResultData.Error(error))
        val event = PlacesIntent.FindNearbyPlaces(fakeLatitude,fakeLongitude)

        coEvery { findNearbyPlacesUseCase.invoke(fakeLatitude,fakeLongitude) } returns flowError

        viewModel.execute(event)

        Assert.assertEquals(2, mutableListStates.size)
        Assert.assertEquals((slot.captured as PlacesState.Error).message, error)
    }


}