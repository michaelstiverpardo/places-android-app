package com.mistpaag.favoriteplaces.parcelables

import android.os.Parcelable
import com.mistpaag.domain.Place
import kotlinx.parcelize.Parcelize

@Parcelize
data class PlaceParcelable(
    val id: String,
    val name: String,
    val categoryName: String?,
    val categoryImage: String?,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val distance: Int,
): Parcelable

fun Place.toParcelable() : PlaceParcelable{
    return PlaceParcelable(
        id=id,
        name=name,
        categoryName=categoryName,
        categoryImage=categoryImage,
        address=address,
        latitude=latitude,
        longitude=longitude,
        distance=distance
    )
}

fun PlaceParcelable.toDomain() : Place{
    return Place(
        id=id,
        name=name,
        categoryName=categoryName,
        categoryImage=categoryImage,
        address=address,
        latitude=latitude,
        longitude=longitude,
        distance=distance
    )
}