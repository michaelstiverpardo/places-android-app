package com.mistpaag.favoriteplaces.presentation.detail

import com.mistpaag.domain.Place

sealed class PlaceDetailState {
    data class PlaceDetail(val place: Place): PlaceDetailState()
}