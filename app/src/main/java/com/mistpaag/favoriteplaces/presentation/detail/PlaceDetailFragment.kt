package com.mistpaag.favoriteplaces.presentation.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mistpaag.domain.Place
import com.mistpaag.favoriteplaces.R
import com.mistpaag.favoriteplaces.databinding.PlaceDetailFragmentBinding
import com.mistpaag.favoriteplaces.parcelables.toDomain
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class PlaceDetailFragment : Fragment() {

    companion object {
        fun newInstance() = PlaceDetailFragment()
    }

    private lateinit var binding: PlaceDetailFragmentBinding

    private val viewModel by viewModels<PlaceDetailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PlaceDetailFragmentBinding.inflate(inflater, container, false)

        setupUI()

        return binding.root
    }

    private fun setupUI() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is PlaceDetailState.PlaceDetail -> setPlace(state.place)
            }
        }
    }

    private fun setPlace(place: Place){
        binding.apply {
            tittlePlace.text = place.name
            subTittlePlace.text = place.categoryName
            distancePlace.text = getString(R.string.distance_value,place.distance.toString())
            (childFragmentManager.findFragmentById(R.id.details_map) as SupportMapFragment).getMapAsync { googleMap ->
                val location = LatLng(place.latitude, place.longitude)
                googleMap.addMarker(
                    MarkerOptions()
                        .position(location)
                        .title(place.name)
                )
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location,17f))

            }
        }
    }

    override fun onStart() {
        super.onStart()
        getArguments(arguments)
    }

    private fun getArguments(arguments: Bundle?) {
        arguments?.let {
            val pokemon = PlaceDetailFragmentArgs.fromBundle(it).place.toDomain()
            viewModel.execute(PlaceDetailIntent.ConfigurePlace(pokemon))
        }
    }

}