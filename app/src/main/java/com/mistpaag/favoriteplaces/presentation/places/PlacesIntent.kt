package com.mistpaag.favoriteplaces.presentation.places

sealed class PlacesIntent {
    data class FindNearbyPlaces(val latitude:Double, val longitude: Double): PlacesIntent()
}