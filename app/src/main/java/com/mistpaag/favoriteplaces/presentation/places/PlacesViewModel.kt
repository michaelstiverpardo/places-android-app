package com.mistpaag.favoriteplaces.presentation.places

import androidx.lifecycle.viewModelScope
import com.mistpaag.data.ResultData
import com.mistpaag.favoriteplaces.base.BaseViewModel
import com.mistpaag.usecases.FindNearbyPlacesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PlacesViewModel @Inject constructor(
    private val findNearbyPlacesUseCase: FindNearbyPlacesUseCase
) : BaseViewModel<PlacesState>() {

    fun execute(intent: PlacesIntent){
        _state.value = PlacesState.Loading
        when (intent){
            is PlacesIntent.FindNearbyPlaces -> findNearbyPlaces(intent.
            latitude, intent.longitude)
        }
    }

    private fun findNearbyPlaces(latitude: Double, longitude: Double) {
        viewModelScope.launch {
            findNearbyPlacesUseCase.invoke(latitude, longitude).collect { placesData->
                when (placesData){
                    is ResultData.Error -> _state.value = PlacesState.Error(placesData.message)
                    is ResultData.Success -> _state.value = PlacesState.PlacesList(placesData.data)
                }
            }
        }
    }

}