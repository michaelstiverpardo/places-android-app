package com.mistpaag.favoriteplaces.presentation.detail

import com.mistpaag.domain.Place

sealed class PlaceDetailIntent {
    data class ConfigurePlace(val place: Place): PlaceDetailIntent()
}