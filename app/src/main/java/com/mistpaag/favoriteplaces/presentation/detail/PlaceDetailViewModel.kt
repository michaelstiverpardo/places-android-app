package com.mistpaag.favoriteplaces.presentation.detail

import com.mistpaag.favoriteplaces.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PlaceDetailViewModel @Inject constructor(

) : BaseViewModel<PlaceDetailState>() {
    fun execute(intent: PlaceDetailIntent){
        when (intent){
            is PlaceDetailIntent.ConfigurePlace -> _state.value = PlaceDetailState.PlaceDetail(intent.place)
        }
    }
}