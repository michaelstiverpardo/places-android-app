package com.mistpaag.favoriteplaces.presentation.places

import com.mistpaag.domain.Place

sealed class PlacesState {
    object Loading: PlacesState()
    data class PlacesList(val list: List<Place>): PlacesState()
    data class Error(val message: String): PlacesState()
}