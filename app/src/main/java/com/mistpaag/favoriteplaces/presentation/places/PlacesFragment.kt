package com.mistpaag.favoriteplaces.presentation.places

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.mistpaag.commons.location.CurrentLocationManager
import com.mistpaag.commons.permission.PermissionManager
import com.mistpaag.domain.Place
import com.mistpaag.favoriteplaces.databinding.PlacesFragmentBinding
import com.mistpaag.favoriteplaces.parcelables.toParcelable
import com.mistpaag.favoriteplaces.presentation.places.adapter.PlacesAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PlacesFragment : Fragment() {

    private lateinit var adapter : PlacesAdapter

    @Inject
    lateinit var permissionManager: PermissionManager

    @Inject
    lateinit var currentLocationManager: CurrentLocationManager

    companion object {
        fun newInstance() = PlacesFragment()
    }

    private val viewModel by viewModels<PlacesViewModel>()
    private lateinit var binding: PlacesFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PlacesFragmentBinding.inflate(inflater, container, false)

        setupUI()

        return binding.root
    }

    private fun setupUI() {
        setupRecycler()
        permissionManager.giveAppPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
            isGranted = { getLocation() },
            isRefused = { Toast.makeText(requireContext(),"Refussed", Toast.LENGTH_LONG).show()
        })

        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is PlacesState.Error -> Toast.makeText(requireContext(), state.message, Toast.LENGTH_LONG).show()
                PlacesState.Loading -> {}
                is PlacesState.PlacesList -> setList(state.list.sortedBy { it.distance })
            }
        }
    }

    private fun setupRecycler(){
        adapter = PlacesAdapter { navigateToDetail(it) }
        binding.placesRecycler.adapter = adapter
    }

    private fun setList(list: List<Place>){
        adapter.submitList(list.toMutableList())
    }

    private fun navigateToDetail(place: Place){
        findNavController().navigate(PlacesFragmentDirections.actionPlacesFragmentToPlaceDetailFragment(place.toParcelable()))
    }

    private fun getLocation(){
        currentLocationManager.getCurrentLocation{ location ->
            with(location){
                viewModel.execute(PlacesIntent.FindNearbyPlaces(latitude,longitude))
            }
        }
    }


}