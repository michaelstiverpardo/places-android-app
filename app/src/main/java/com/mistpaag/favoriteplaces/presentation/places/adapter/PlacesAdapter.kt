package com.mistpaag.favoriteplaces.presentation.places.adapter

import android.content.ContentResolver
import android.provider.Settings.Global.getString
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.ui.res.stringResource
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mistpaag.domain.Place
import com.mistpaag.favoriteplaces.R
import com.mistpaag.favoriteplaces.databinding.PlaceItemBinding
import com.mistpaag.favoriteplaces.utils.DiffCallback
import com.mistpaag.imagemanager.loadImage
import kotlinx.coroutines.withContext

class PlacesAdapter(val itemClick: (Place) -> Unit): ListAdapter<Place, PlacesAdapter.ViewHolder>(
    DiffCallback(
        {
            old: Place, new: Place ->
            old.id == new.id

        },
        {
            old, new ->
            old == new
        }
    )
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            PlaceItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }





    inner class ViewHolder(private val binding: PlaceItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bindTo(item: Place){
            binding.apply {
                tittlePlace.text = item.name
                subTittlePlace.text = item.categoryName
                item.categoryImage?.let { iconPlace.loadImage(it) }
                distancePlace.text = distancePlace.context.getString(R.string.distance_value,item.distance.toString())
                placeContainer.setOnClickListener {
                    itemClick(item)
                }
            }
        }
    }
}
