package com.mistpaag.domain

data class Place(
    val id: String,
    val name: String,
    val categoryName: String?,
    val categoryImage: String?,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val distance: Int,
)