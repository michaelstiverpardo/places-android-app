# Foursquare App
A simple Android app for The [Foursquare](https://foursquare.com/) API 

## Development setup

You require the latest Android Studio Artict Fox to be able to build the app.

### Libraries

- Application entirely written in [Kotlin](https://kotlinlang.org)
- Asynchronous processing using [Coroutines](https://kotlin.github.io/kotlinx.coroutines/)
- Uses [Hilt](https://dagger.dev/hilt/) for dependency injection
- [Room](https://developer.android.com/jetpack/androidx/releases/room) for persistence
- [Retrofit](https://square.github.io/retrofit/) for api request
- [Navigation](https://developer.android.com/guide/navigation) for fragment navigation

### API keys

You need to supply your API / client keys for the service the app uses.

- [Foursquare](https://foursquare.com/products/places-api/)
- [Google Maps](https://developers.google.com/maps/documentation/android-sdk/start)

Once you obtain the key, you can set them in your `~/local.properties`:

```
# Get this from APIKeys Section
foursquare.key=<your_api_key>
apibase.key=<api_base_url_foursquare>
maps.key=<your_api_key>
```

