package com.mistpaag.commons.utils

import android.location.Location

fun getDistance(currentLat:Double,currentLng:Double,lat:Double,lng:Double): Double {
    val startPoint = Location("locationA")
    startPoint.latitude = currentLat
    startPoint.longitude = currentLng

    val endPoint = Location("locationB")
    endPoint.latitude = lat
    endPoint.longitude = lng
    val distance = distance(
        startPoint.latitude,startPoint.longitude,
        endPoint.latitude,endPoint.longitude,
    )
    return (distance * 1000.0).toKm()


}

fun Double.toKm() = this * 1.60934

private fun distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
    val theta = lon1 - lon2
    var dist = (Math.sin(deg2rad(lat1))
            * Math.sin(deg2rad(lat2))
            + (Math.cos(deg2rad(lat1))
            * Math.cos(deg2rad(lat2))
            * Math.cos(deg2rad(theta))))
    dist = Math.acos(dist)
    dist = rad2deg(dist)
    dist *= 60 * 1.1515
    return dist
}

private fun deg2rad(deg: Double): Double {
    return deg * Math.PI / 180.0
}

private fun rad2deg(rad: Double): Double {
    return rad * 180.0 / Math.PI
}