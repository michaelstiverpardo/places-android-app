package com.mistpaag.commons.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.core.app.ActivityCompat

class CurrentLocationManagerImpl(
    private val context: Context
) : CurrentLocationManager {
    lateinit var mLocationManager: LocationManager
    lateinit var mLocationListener: LocationListener

    companion object {
        const val MIN_TIME = 50000
        const val MIN_DISTANCE = 10000
        const val LOCATION_PROVIDER = LocationManager.GPS_PROVIDER
    }
    
    @SuppressLint("MissingPermission")
    override fun getCurrentLocation(currentLocation: (Location) -> Unit) {
        mLocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        mLocationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                currentLocation(location)
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            }

            override fun onProviderDisabled(provider: String) {

            }

            override fun onProviderEnabled(provider: String) {

            }
        }
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        mLocationManager.requestLocationUpdates(
            LOCATION_PROVIDER,
            MIN_TIME.toLong(),
            MIN_DISTANCE.toFloat(),
            mLocationListener
        )
    }
}

interface CurrentLocationManager{
    fun getCurrentLocation(currentLocation: (Location) -> Unit)
}