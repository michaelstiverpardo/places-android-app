package com.mistpaag.commons.di

import android.content.Context
import com.mistpaag.commons.location.CurrentLocationManager
import com.mistpaag.commons.location.CurrentLocationManagerImpl
import com.mistpaag.commons.permission.PermissionManager
import com.mistpaag.commons.permission.PermissionManagerImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object CommonsModule {
    @Provides
    fun providesPermissionManager(
    ): PermissionManager = PermissionManagerImpl(
    )

    @Provides
    fun providesCurrentLocationManager(
        @ApplicationContext context: Context
    ): CurrentLocationManager = CurrentLocationManagerImpl(
        context = context
    )
}