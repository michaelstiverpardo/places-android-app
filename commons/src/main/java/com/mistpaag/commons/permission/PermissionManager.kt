package com.mistpaag.commons.permission

import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment

class PermissionManagerImpl : PermissionManager {

    override fun giveAppPermission(
        fragment: Fragment,
        permissionStr: String,
        isGranted: () -> Unit,
        isRefused: () -> Unit
    ) {
        fragment.registerForActivityResult(ActivityResultContracts.RequestPermission()){ isGranted->
            if (isGranted){
                isGranted()
            }else{
                isRefused()
            }
        }.launch(permissionStr)
    }

    override fun giveAppPermissions(
        fragment: Fragment,
        permissionsArray: Array<String>,
        isGranted: () -> Unit,
        isRefused: () -> Unit
    ) {
        fragment.registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()){ permissions->
            if (!permissions.values.contains(false)){
                isGranted()
            }else{
                isRefused()
            }
        }.launch(permissionsArray)
    }

}

interface PermissionManager{
    fun giveAppPermission(fragment: Fragment, permissionStr: String, isGranted: () -> Unit, isRefused: () -> Unit)
    fun giveAppPermissions(fragment: Fragment, permissionsArray: Array<String>, isGranted: () -> Unit, isRefused: () -> Unit)
}