package com.mistpaag.databasemanager.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.mistpaag.commons.utils.getDistance
import com.mistpaag.domain.Place

@Entity(tableName = "places")
data class PlaceLocal(
    @PrimaryKey val id: String,
    val name: String,
    val categoryName: String?,
    val categoryImage: String?,
    val address: String,
    val latitude: Double,
    val longitude: Double,
)

fun Place.toLocal() : PlaceLocal{
    return PlaceLocal(
        id=id,
        name=name,
        categoryName=categoryName,
        categoryImage=categoryImage,
        address=address,
        latitude=latitude,
        longitude=longitude,
    )
}

fun List<Place>.toLocal() : List<PlaceLocal>{
    return this.map { it.toLocal() }
}

fun PlaceLocal.toDomain(currentLatitude: Double, currentLongitude: Double) : Place{
    return Place(
        id=id,
        name=name,
        categoryName=categoryName,
        categoryImage=categoryImage,
        address=address,
        latitude=latitude,
        longitude=longitude,
        distance = getDistance(currentLatitude, currentLongitude, latitude, longitude).toInt()
    )
}



fun List<PlaceLocal>.toDomainWithCurrentLocation(currentLatitude: Double, currentLongitude: Double) : List<Place>{
    return this.map { it.toDomain(currentLatitude, currentLongitude) }
}