package com.mistpaag.databasemanager.dao

import androidx.room.*
import com.mistpaag.databasemanager.model.PlaceLocal

@Dao
interface PlaceDao {
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(users: List<PlaceLocal>)

    @Query("SELECT * FROM places")
    suspend fun getAll(): List<PlaceLocal>

    @Query("DELETE FROM places")
    suspend fun clearAll()
}

