package com.mistpaag.databasemanager.di

import android.content.Context
import com.mistpaag.data.source.PlaceLocalSource
import com.mistpaag.databasemanager.PlaceLocalDataSource
import com.mistpaag.databasemanager.db.PlaceDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MovieLocalModule {
    @Singleton
    @Provides
    fun providesPokemonDB( @ApplicationContext context: Context): PlaceDB {
        return PlaceDB.build(context, DB_NAME)
    }

    @Singleton
    @Provides
    fun providesLocalSource(
        db: PlaceDB
    ): PlaceLocalSource {
        return PlaceLocalDataSource(db)
    }


}
private const val DB_NAME = "place_db"