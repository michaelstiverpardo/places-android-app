package com.mistpaag.databasemanager

import com.mistpaag.commons.utils.getDistance
import com.mistpaag.data.ResultData
import com.mistpaag.data.source.PlaceLocalSource
import com.mistpaag.databasemanager.db.PlaceDB
import com.mistpaag.databasemanager.model.toDomainWithCurrentLocation
import com.mistpaag.databasemanager.model.toLocal
import com.mistpaag.domain.Place
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class PlaceLocalDataSource @Inject constructor(
    db: PlaceDB,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
): PlaceLocalSource {
    private val placeDao = db.placeDao
    override fun findNearbyPlaces(latitude:Double, longitude: Double): Flow<ResultData<List<Place>>> = flow {
        val places = placeDao.getAll().filter { getDistance(latitude,longitude,it.latitude, it.longitude) < 200 }.toDomainWithCurrentLocation(latitude,longitude)
        emit(
            ResultData.Success(
                places
            )
        )
    }.flowOn(dispatcher)

    override fun savePlaces(places: List<Place>): Flow<ResultData<List<Place>>> = flow<ResultData<List<Place>>> {
        placeDao.insertAll(places.toLocal())
        emit(ResultData.Success(places))
    }.flowOn(dispatcher)

}