package com.mistpaag.databasemanager.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mistpaag.databasemanager.dao.PlaceDao
import com.mistpaag.databasemanager.model.PlaceLocal

@Database(entities = [
    PlaceLocal::class,
], version =1, exportSchema = false)
abstract class PlaceDB : RoomDatabase() {
    companion object {
        fun build(context: Context, dbName: String) = Room.databaseBuilder(
            context,
            PlaceDB::class.java,
            dbName
        ).build()
    }

    //    Movies
    abstract val placeDao: PlaceDao
}