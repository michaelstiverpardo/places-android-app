package com.mistpaag.requestmanager

import com.mistpaag.data.ResultData
import com.mistpaag.data.source.PlaceRemoteSource
import com.mistpaag.domain.Place
import com.mistpaag.requestmanager.endpoint.PlaceService
import com.mistpaag.requestmanager.model.ResultsPlacesRemote
import com.mistpaag.requestmanager.model.toDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class PlaceRemoteDataSource @Inject constructor(
    private val placeService: PlaceService,
    private val coroutineDispatcher: CoroutineDispatcher = Dispatchers.IO,
) : PlaceRemoteSource {
    private val errorMessage = "Something failed calling service"

    override fun findNearbyPlaces(latitude:Double, longitude: Double): Flow<ResultData<List<Place>>> = flow {
        emit(
            renderData(
                safeApiCall(
                    call = { placeService.findNearbyPlaces("$latitude,$longitude").callServices() },
                    errorMessage = errorMessage
                )
            )
        )
    }.flowOn(coroutineDispatcher)

    private fun renderData(resultDataList: ResultData<ResultsPlacesRemote>): ResultData<List<Place>> {
        return when (resultDataList){
            is ResultData.Error -> ResultData.Error(resultDataList.message)
            is ResultData.Success -> ResultData.Success(resultDataList.data.placeRemotes.toDomain())
        }
    }
}