package com.mistpaag.requestmanager.model


import com.google.gson.annotations.SerializedName

data class ResultsPlacesRemote(
    @SerializedName("results")
    val placeRemotes: List<PlaceRemote>
)