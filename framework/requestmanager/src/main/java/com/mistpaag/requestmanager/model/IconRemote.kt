package com.mistpaag.requestmanager.model


import com.google.gson.annotations.SerializedName

data class IconRemote(
    @SerializedName("prefix")
    val prefix: String,
    @SerializedName("suffix")
    val suffix: String
)