package com.mistpaag.requestmanager.model


import com.google.gson.annotations.SerializedName

data class CategoryRemote(
    @SerializedName("icon")
    val iconRemote: IconRemote,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)

fun List<CategoryRemote>.getFirstCategory(): CategoryRemote? {
    return if (size > 0){
        this[0]
    }else{
        null
    }
}

fun List<CategoryRemote>.getFirstCategoryName(): String? {
    return getFirstCategory()?.name
}

fun List<CategoryRemote>.getFirstCategoryImage(): String? {
    return getFirstCategory()?.let {
        it.iconRemote.prefix + "88" + it.iconRemote.suffix
    }
}

