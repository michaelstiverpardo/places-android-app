package com.mistpaag.requestmanager.di

import com.mistpaag.data.source.PlaceRemoteSource
import com.mistpaag.requestmanager.PlaceRemoteDataSource
import com.mistpaag.requestmanager.endpoint.PlaceService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object PlaceRemoteModule {

    @Provides
    fun providesPokemonRemoteSource(
        placeService: PlaceService
    ): PlaceRemoteSource = PlaceRemoteDataSource(
        placeService= placeService
    )
}