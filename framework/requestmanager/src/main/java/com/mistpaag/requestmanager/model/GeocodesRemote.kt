package com.mistpaag.requestmanager.model


import com.google.gson.annotations.SerializedName

data class GeocodesRemote(
    @SerializedName("main")
    val mainRemote: MainRemote
)