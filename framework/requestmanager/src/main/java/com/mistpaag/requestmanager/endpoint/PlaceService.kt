package com.mistpaag.requestmanager.endpoint

import com.mistpaag.requestmanager.model.ResultsPlacesRemote
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PlaceService {
    @GET("places/nearby")
    suspend fun findNearbyPlaces(
        @Query("ll") latLong:String,
        @Query("limit") limit:Int = 50
    ) : Response<ResultsPlacesRemote>
}