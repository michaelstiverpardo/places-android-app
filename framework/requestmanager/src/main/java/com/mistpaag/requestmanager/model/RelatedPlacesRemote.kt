package com.mistpaag.requestmanager.model


import com.google.gson.annotations.SerializedName

data class RelatedPlacesRemote(
    @SerializedName("children")
    val children: List<ChildrenRemote>,
    @SerializedName("parent")
    val parentRemote: ParentRemote
)