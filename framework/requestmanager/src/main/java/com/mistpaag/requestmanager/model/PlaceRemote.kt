package com.mistpaag.requestmanager.model


import com.google.gson.annotations.SerializedName
import com.mistpaag.domain.Place

data class PlaceRemote(
    @SerializedName("categories")
    val categoryRemotes: List<CategoryRemote>,
    @SerializedName("chains")
    val chains: List<Any>,
    @SerializedName("distance")
    val distance: Int,
    @SerializedName("fsq_id")
    val fsqId: String,
    @SerializedName("geocodes")
    val geocodesRemote: GeocodesRemote,
    @SerializedName("location")
    val locationRemote: LocationRemote,
    @SerializedName("name")
    val name: String,
    @SerializedName("related_places")
    val relatedPlacesRemote: RelatedPlacesRemote,
    @SerializedName("timezone")
    val timezone: String
)

fun PlaceRemote.toDomain(): Place {
    return Place(
        name = name,
        id= fsqId,
        categoryName= categoryRemotes.getFirstCategoryName(),
        categoryImage= categoryRemotes.getFirstCategoryImage(),
        address= locationRemote.formattedAddress,
        latitude= geocodesRemote.mainRemote.latitude,
        longitude= geocodesRemote.mainRemote.longitude,
        distance = distance
    )
}

fun List<PlaceRemote>.toDomain(): List<Place> {
    return map { it.toDomain() }
}
