package com.mistpaag.requestmanager.model


import com.google.gson.annotations.SerializedName

data class LocationRemote(
    @SerializedName("address")
    val address: String,
    @SerializedName("country")
    val country: String,
    @SerializedName("cross_street")
    val crossStreet: String,
    @SerializedName("dma")
    val dma: String,
    @SerializedName("formatted_address")
    val formattedAddress: String,
    @SerializedName("locality")
    val locality: String,
    @SerializedName("neighborhood")
    val neighborhood: List<String>,
    @SerializedName("postcode")
    val postcode: String,
    @SerializedName("region")
    val region: String
)