package com.mistpaag.requestmanager.model


import com.google.gson.annotations.SerializedName

data class ParentRemote(
    @SerializedName("fsq_id")
    val fsqId: String,
    @SerializedName("name")
    val name: String
)