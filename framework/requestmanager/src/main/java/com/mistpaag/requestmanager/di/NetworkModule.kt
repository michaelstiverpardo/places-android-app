package com.mistpaag.requestmanager.di

import android.content.Context
import com.mistpaag.requestmanager.R
import com.mistpaag.requestmanager.RetrofitBuild
import com.mistpaag.requestmanager.endpoint.PlaceService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun providesRetrofitBuild(
        @ApplicationContext context: Context
    ): Retrofit {
        return RetrofitBuild(context.getString(R.string.api_base_url),context.getString(R.string.foursquare_key)).retrofit
    }

    @Provides
    fun providesPlaceService(
        retrofit: Retrofit
    ): PlaceService {
        return retrofit.create(
            PlaceService::class.java
        )
    }

}