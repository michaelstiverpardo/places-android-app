package com.mistpaag.sharedtesting

import com.mistpaag.domain.Place

object FakePlaceValues {
    fun getLatitude() = 37.421998333333335
    fun getError() = "error"
    fun getLongitude() = -122.08400000000002
    fun getPlaces() = listOf(getPlace())
    fun getPlace() = Place("id","name", "categoryName","categoryImage", "address", getLatitude(), getLongitude(), 83)
}