package com.mistpaag.data.source

import com.mistpaag.data.ResultData
import com.mistpaag.domain.Place
import kotlinx.coroutines.flow.Flow


interface PlaceLocalSource {
    fun findNearbyPlaces(latitude:Double, longitude: Double): Flow<ResultData<List<Place>>>
    fun savePlaces(places: List<Place>): Flow<ResultData<List<Place>>>
}