package com.mistpaag.data

import com.mistpaag.domain.Place
import kotlinx.coroutines.flow.Flow

interface PlaceRepository {
    fun findNearbyPlaces(latitude:Double, longitude: Double) : Flow<ResultData<List<Place>>>
}