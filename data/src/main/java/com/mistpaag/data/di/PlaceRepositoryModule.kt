package com.mistpaag.data.di

import com.mistpaag.data.PlaceRepository
import com.mistpaag.data.PlaceRepositoryImpl
import com.mistpaag.data.source.ConnectivityChecker
import com.mistpaag.data.source.PlaceLocalSource
import com.mistpaag.data.source.PlaceRemoteSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object PlaceRepositoryModule {
    @Provides
    fun providesPlaceRepository(
        placeRemoteSource: PlaceRemoteSource,
        placeLocalSource: PlaceLocalSource,
        connectivityChecker: ConnectivityChecker
    ): PlaceRepository = PlaceRepositoryImpl(
        remoteSource = placeRemoteSource,
        localSource = placeLocalSource,
        connectivityChecker = connectivityChecker
    )
}