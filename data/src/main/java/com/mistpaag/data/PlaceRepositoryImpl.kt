package com.mistpaag.data

import com.mistpaag.data.source.ConnectivityChecker
import com.mistpaag.data.source.PlaceLocalSource
import com.mistpaag.data.source.PlaceRemoteSource
import com.mistpaag.domain.Place
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class PlaceRepositoryImpl @Inject constructor(
    private val remoteSource: PlaceRemoteSource,
    private val localSource: PlaceLocalSource,
    private val connectivityChecker: ConnectivityChecker
): PlaceRepository {

    override fun findNearbyPlaces(latitude:Double, longitude: Double): Flow<ResultData<List<Place>>> = flow {
        if (connectivityChecker.isInternetAvailable()){
            remoteSource.findNearbyPlaces(latitude, longitude).collect {
                when(it){
                    is ResultData.Error -> {
                        localSource.findNearbyPlaces(latitude, longitude).collect { emit(it) }
                    }
                    is ResultData.Success -> {
                        localSource.savePlaces(it.data).collect { _ ->
                            emit(it)
                        }
                    }
                }
            }
        }else{
            localSource.findNearbyPlaces(latitude, longitude).collect { emit(it) }
        }
    }
}