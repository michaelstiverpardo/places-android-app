package com.mistpaag.data

import com.mistpaag.data.source.ConnectivityChecker
import com.mistpaag.data.source.PlaceLocalSource
import com.mistpaag.data.source.PlaceRemoteSource
import com.mistpaag.sharedtesting.CoroutineTestRule
import com.mistpaag.sharedtesting.FakePlaceValues
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class PlaceRepositoryImplTest{
    @get:Rule
    var coroutineTestRule = CoroutineTestRule()

    private lateinit var repository: PlaceRepository

    @RelaxedMockK
    lateinit var remoteSource: PlaceRemoteSource

    @RelaxedMockK
    lateinit var localSource: PlaceLocalSource

    @RelaxedMockK
    lateinit var connectivityChecker: ConnectivityChecker

    @Before
    fun setup(){
        MockKAnnotations.init(this, relaxUnitFun = true, relaxed = true)
        repository = PlaceRepositoryImpl(remoteSource,localSource, connectivityChecker)
    }

    @Test
    fun `GIVEN internetAvailable WHEN findNearbyPlaces THEN returnSuccessList`() = runBlockingTest{
        val fakeLatitude = FakePlaceValues.getLatitude()
        val fakeLongitude = FakePlaceValues.getLongitude()
        val response = FakePlaceValues.getPlaces()
        val flowResponse = flowOf(ResultData.Success(response))
        every { connectivityChecker.isInternetAvailable() } returns true
        coEvery { remoteSource.findNearbyPlaces(fakeLatitude, fakeLongitude) } returns flowResponse
        coEvery { localSource.savePlaces(response) } returns flowResponse

        val result = repository.findNearbyPlaces(fakeLatitude, fakeLongitude)


        result.collect{ placesData->
            coVerify(exactly = 1) {
                remoteSource.findNearbyPlaces(any(),any())
                localSource.savePlaces(any())
            }
            assert(placesData.isSuccess())
            assertEquals(placesData.getSuccess()?.first(), response.first())
        }
    }

    @Test
    fun `GIVEN internetAvailable WHEN findNearbyPlaces THEN localSource returnSuccessList`() = runBlockingTest{
        val fakeLatitude = FakePlaceValues.getLatitude()
        val fakeLongitude = FakePlaceValues.getLongitude()
        val response = FakePlaceValues.getPlaces()
        val flowResponse = flowOf(ResultData.Success(response))
        val error = FakePlaceValues.getError()
        val flowError = flowOf(ResultData.Error(error))
        every { connectivityChecker.isInternetAvailable() } returns true
        coEvery { remoteSource.findNearbyPlaces(fakeLatitude, fakeLongitude) } returns flowError
        coEvery { localSource.findNearbyPlaces(fakeLatitude,fakeLongitude) } returns flowResponse

        val result = repository.findNearbyPlaces(fakeLatitude, fakeLongitude)


        result.collect{ placesData->
            coVerify(exactly = 1) {
                remoteSource.findNearbyPlaces(any(),any())
                localSource.findNearbyPlaces(any(),any())
            }
            assert(placesData.isSuccess())
            assertEquals(placesData.getSuccess()?.first(), response.first())
        }
    }

    @Test
    fun `GIVEN internetUnavailable WHEN findNearbyPlaces THEN returnSuccessList`() = runBlockingTest{
        val fakeLatitude = FakePlaceValues.getLatitude()
        val fakeLongitude = FakePlaceValues.getLongitude()

        val response = FakePlaceValues.getPlaces()
        val flowResponse = flowOf(ResultData.Success(response))
        every { connectivityChecker.isInternetAvailable() } returns true
        coEvery { localSource.findNearbyPlaces(fakeLatitude, fakeLongitude) } returns flowResponse

        val result = repository.findNearbyPlaces(fakeLatitude, fakeLongitude)

        result.collect{ placesData->
            coVerify(exactly = 0) {
                remoteSource.findNearbyPlaces(any(),any())
                localSource.savePlaces(any())
            }
            coVerify(exactly = 1) {
                localSource.findNearbyPlaces(any(),any())
            }
            assert(placesData.isSuccess())
            assertEquals(placesData.getSuccess()?.first(), response.first())
        }
    }
}